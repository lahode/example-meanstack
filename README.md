--------------------
Présentation de Mean
--------------------

Pour plus d'information sur Mean, voici la présentation:

https://prezi.com/_n0hvme5chwh/presentation-de-mean/

---------------------
Télécharger le projet
---------------------

git clone git@gitlab.com:lahode/example-meanstack.git

(sur un Linux ou un Mac)

-----------------------
Installation de MongoDB
-----------------------

curl -O https://fastdl.mongodb.org/osx/mongodb-osx-x86_64-3.4.1.tgz

tar -zxvf mongodb-osx-x86_64-3.4.1.tgz

mv mongodb-osx-x86_64-3.4.1/ mongodb

mkdir mongo-data

Et lancer mongo avec: ~/mongo/bin/mongod --dbpath ~/mongo-data

Plus d'info sur: https://docs.mongodb.com/manual/tutorial/

On peut bien sûr lancer mongoDB avec un daemon pour qu'il tourne en arrière plan
(Ex. sur Ubuntu 16.04: https://www.digitalocean.com/community/tutorials/how-to-install-mongodb-on-ubuntu-16-04)

----------------------
Installation de NodeJS
----------------------

- Mettre à jour 

apt-get update

- Installer NVM (Node version manager, vérifier s'il existe une version plus récente)

curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.0/install.sh | bash

- Choisir la version 6 de NodeJS (NPM : Node package manager est inclus avec NodeJS)

nvm install v6

- Installer Nodemon permet de lancer un processus node qui se relancer à chaque changement de code

npm install nodemon -g

-----------------------------
Installer Express et Mongoose
-----------------------------

- Installer Express

npm install express -g

- Installer Mongoose

npm install mongoose -g

Il y a MongoExpress qui est intéressant pour avoir un "PHPMyadmin like" pour MongoDB:
https://github.com/mongo-express/mongo-express

------------------
Lancer le back-end
------------------

- Initialisez le projet pour charger les modules liés

cd demo-dgsi-server

npm install

2. Lancer le projet (par défaut elle est configurée dans le index.js sur le port 4500):

node index.js (ou nodemon index.js)

---------------------------------
Installer Angular 2 et typescript
---------------------------------
- Installer la console d'angular2

npm install -g angular-cli

- Installer typescript

npm install typescript -g

-------------------
Lancer le front-end
-------------------
- Initialisez le projet pour charger les modules liés

cd demo-dgsi-ng

npm install

- Lancer le projet:

ng serve

L'application sera accessible sur http://localhost:4200
------------------
------------------
Bon dév ;)
