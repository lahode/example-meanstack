import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { Element } from './element.interface';
import { NKDatetimeModule } from 'ng2-datetime/ng2-datetime';
import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent implements OnInit {

  public element: Element;
  public elements: Element[];
  public message: string;

  constructor(private appService: AppService) {
    this.element = {_id: null, date: new Date(), titre: '', texte: ''};
  }

  // Récupère tous les éléments lors de l'initialisation
  public ngOnInit() {
    this.getAll();
  }

  // Défini l'élément à modifier
  public onEdit(element) {
    this.element = element;
  }

  // Execute la sauvegarde de l'élément
  public onSave() {
    this.appService.save(this.element).subscribe(
      retour => {
        this.message = retour.message;
        this.element = {_id: null, date: new Date(), titre: '', texte: ''};
        this.getAll();
      },
      error => this.message = error._body
    );
  }

  // Récupère un élément
  public onDelete(element: Element) {
    this.appService.delete(element._id).subscribe(
      retour => {
        this.message = retour.message;
        this.getAll();
      },
      error => this.message = error
    );
  }

  // Récupère tous les éléments
  public getAll() {
    this.appService.all().subscribe(
      retour => this.elements = retour,
      error => this.message = error
    );
  }

}
