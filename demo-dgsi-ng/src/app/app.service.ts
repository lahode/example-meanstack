import { Injectable } from '@angular/core';
import { Http, Response } from "@angular/http";
import { Observable } from "rxjs/Rx";
import { Element } from './element.interface';

@Injectable()
export class AppService {

  private url = 'http://localhost:4500';

  constructor(private http: Http) {}

  /**
   * Récupère tous les éléments
   */
  public all() {
    return this.http.get(this.url + '/all')
      .map((response: Response) => {
        let elements = response.json().elements;
        for(let e in elements) {
          elements[e].date = new Date(elements[e].date);
        }
        return elements;
      })
      .catch((error: Response) => Observable.throw(error));
  };

  /**
   * Récupère l'élément par son ID
   */
  public delete(id: string) {
    return this.http.get(this.url + '/delete/' + id)
      .map((response: Response) => {
        return response;
      })
      .catch((error: Response) => Observable.throw(error));
  };

  /**
   * Sauvegarde l'élément
   */
  public save(element: Element) {
    return this.http.post(this.url + '/save', element)
      .map((response: Response) => {
        return response;
      })
      .catch((error: Response) => Observable.throw(error));
  };

}
