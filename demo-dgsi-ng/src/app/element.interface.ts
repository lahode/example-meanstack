export interface Element {
  _id: string
  date: Date;
  titre: string;
  texte: string;
}