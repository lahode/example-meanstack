import { DemoDgsiPage } from './app.po';

describe('demo-dgsi App', function() {
  let page: DemoDgsiPage;

  beforeEach(() => {
    page = new DemoDgsiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
