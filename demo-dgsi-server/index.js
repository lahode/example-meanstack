/**
 * Importation des libraries
 */
const express     = require('express');
const mongoose    = require('mongoose');
const bodyParser  = require('body-parser');
const _ = require('lodash');

/**
 * Initialise la base de données
 */
mongoose.Promise = global.Promise;
mongoose.connect("mongodb://localhost:27017/demo-dgsi");

/**
 * Initialise le serveur web avec Express
 */
var app = express();

/**
 * Créer le schéma de la collection "Element"
 */
var schemaElement = new mongoose.Schema({
  date: {
    type: Date,
    required: true,
    default: Date.now
  },
  titre: {
    type: String,
    required: true,
    minlength: 3,
    trim: true
  },
  texte: {
    type: String
  },
});
var Element = mongoose.model('Element', schemaElement);

/**
 * Permet de récupère le body HTML en Json
 */
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

/**
 * Autorise les connexions CORS depuis tous les serveur, les method GET et POST et les headers "Content-Type"
 */
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "POST, GET");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

/**
 * Retourne tous les éléments
 */
app.get('/all', (req, res) => {
  Element.find({})
    .then((elementsFound) => {
      if (!elementsFound) {
        return res.status(404).send( "Les éléments n'ont pas été trouvés" );
      }
    res.status(200).json( {elements: elementsFound} );
    }).catch((err) => res.status(400).send( "Une erreur est survenue lors de la récupération des éléments" ));
});

/**
 * Sauvegarde un élément
 */
app.post('/save', (req, res) => {
  let element = _.pick(req.body, '_id', 'date', 'titre', 'texte');

  // Assigne toutes les valeurs en entrée à l'objet Element
  elementToSave = new Element();
  Object.keys(element).forEach((key) => {
    if (element[key]) {
      elementToSave[key] = element[key];
    }
  });

  // Met à jour l'élément dans la base de donnée
  if (element._id) {
    Element.findByIdAndUpdate(element._id, element).then((elementSaved) => {
      res.status(200).send( "L'élément a été modifié" );
    }).catch((err) => res.status(400).send( `Une erreur est survenue lors de la mise à jour de l'élément: ${err.message}` ));
  }
  // Crée l'élément dans la base de donnée
  else {
    elementToSave.save().then((elementSaved) => {
      res.status(200).send( "L'élément a été créé" );
    }).catch((err) => res.status(400).send( `Une erreur est survenue lors de la création de l'élément: ${err.message}` ));
  }
});

/**
 * Retourne un élément par son ID
 */
app.get('/delete/:id', (req, res) => {
  Element.findByIdAndRemove(req.params.id)
    .then((elementDeleted) => {
      res.status(200).send( "L'élément a été supprimé" );
    }).catch((err) => res.status(400).send( "Une erreur est survenue lors de la suppression de l'élément" ));
});

/**
 * Ordonne au serveur d'écouter sur le port 4500
 */
app.listen(4500);

